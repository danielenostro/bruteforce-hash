#!/usr/bin/env python3
# by Dani_xyz

import argparse
import hashlib
import itertools
import string
import time

DESCRIPTION = "This tool performs a bruteforce attack, also known as exhaustive key search, to the target password."

TARGET = "5ebe2294ecd0e0f08eab7690d2a6ee69" # pw: secret

HASH_DICT = {
	"md5": hashlib.md5,
	"sha1": hashlib.sha1,
	"sha256": hashlib.sha256
}

ALPHABET_DICT = {
	"lowercase": string.ascii_lowercase,
	"uppercase": string.ascii_uppercase,
	"letters": string.ascii_letters,
	"digits": string.digits,
	"hexdigits": string.hexdigits,
	"octdigits": string.octdigits,
	"printable": string.printable
}

LENGTH_DICT = {
	"md5": 32,
	"sha1": 40,
	"sha256": 64
}


def gethash(arg):
	try:
		return HASH_DICT[arg]
	except:
		print(f"Unknown hash function: {arg}")
		print("This is a list of known functions:")
		for k in HASH_DICT:
			print('\t' + k)
		exit()


def getalphabet(arg):
	try:
		return ALPHABET_DICT[arg]
	except:
		print(f"Unknown alphabet: {arg}")
		print("This is a list of known alphabets:")
		for k in ALPHABET_DICT:
			print('\t' + k)
		exit()


def crack(target, hash, alphabet, size):
	for x in itertools.product(alphabet, repeat=size):
		pw = ''.join(x)
		if hash(pw.encode('utf-8')).hexdigest() == target:
			return pw
	print(f"no results found for size={size}")
	return crack(target, hash, alphabet, size+1)


def main():
	parser = argparse.ArgumentParser(description=DESCRIPTION)
	parser.add_argument("-H", "--hash", help="set the function used to hash the password (default: md5)", default="md5")
	parser.add_argument("-A", "--alphabet", help="set the alphabet to use (default: lowercase)", default="lowercase")
	parser.add_argument("-S", "--size", help="set the minimum size of the password (default: 1)", type=int, default=1)
	args = parser.parse_args()

	hash = gethash(args.hash)
	alphabet = getalphabet(args.alphabet)

	tlen = len(TARGET)
	hlen = LENGTH_DICT[args.hash]

	if tlen == hlen:
		t0 = time.time()
		print(f"password: {crack(TARGET, hash, alphabet, args.size)}")
		ttc = '%.3f'%(time.time() - t0)
		print(f"{ttc} seconds to crack the password.")
	else:
		print(f"The password is not hashed with {args.hash} function.")
		print(f"{args.hash} produces a {hlen}-digits long hexadecimal digest, but the target is {tlen}.")


if __name__ == "__main__":
	main()