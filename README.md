# Bruteforce hash
A tool to bruteforce hashed passowrds.

### Usage
forcehash.py [-h] [-H HASH] [-A ALPHABET] [-S SIZE]

* -h: prints the help
* -H HASH, --hash HASH sets the function used to hash the password (default: md5)
* -A ALPHABET, --alphabet ALPHABET set the alphabet to use (default: lowercase)
* -S SIZE, --size SIZE set the minimum size of the password (default: 1)